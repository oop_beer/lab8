package com.sittipol.lab8;

public class Map {
    private int width;
    private int height;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Map() {
        this(10, 10);

    }

    public void print() {
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }

}
